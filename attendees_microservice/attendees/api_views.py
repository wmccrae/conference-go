from django.http import JsonResponse
from common.json import ModelEncoder
from django.views.decorators.http import require_http_methods
import json
from .models import Attendee, ConferenceVO, AccountVO

from .poll import get_conferences


class ConferenceVODetailEncoder(ModelEncoder):
    model = ConferenceVO
    properties = ["name", "import_href"]


class AttendeeListEncoder(ModelEncoder):
    model = Attendee
    properties = ["name"]


class AttendeeDetailEncoder(ModelEncoder):
    model = Attendee
    properties = [
        "email",
        "name",
        "company_name",
        "created",
        "conference",
    ]
    encoders = {
        "conference": ConferenceVODetailEncoder(),
    }

    def get_extra_data(self, o):
        # Get the count of AccountVO objects with email equal to o.email
        count = AccountVO.objects.filter(email=o.email).count()
        # Return a dictionary with "has_account": True if count > 0
        # Otherwise, return a dictionary with "has_account": False
        if count > 0:
            # return {"conference": o.conference.name, "has_account": True}
            return {"has_account": True}
        else:
            # return {"conference": o.conference.name, "has_account": False}
            return {"has_account": False}


@require_http_methods({"GET", "POST"})
def api_list_attendees(request, conference_vo_id):
    """
    Lists the attendees names and the link to the attendee
    for the specified conference id.

    Returns a dictionary with a single key "attendees" which
    is a list of attendee names and URLS. Each entry in the list
    is a dictionary that contains the name of the attendee and
    the link to the attendee's information.

    {
        "attendees": [
            {
                "name": attendee's name,
                "href": URL to the attendee,
            },
            ...
        ]
    }
    """
    get_conferences()
    if request.method == "GET":
        # This handles getting and returning the list of attendees for a conf.
        attendees = Attendee.objects.filter(conference=conference_vo_id)
        return JsonResponse(
            attendees,
            encoder=AttendeeListEncoder,
            safe=False,
        )
    else:
        # This handles POST requests to create a new attendee.
        new_attendee = json.loads(request.body)

        # Get the Conference object and put it into the new_attendee dict
        try:
            conference_href = f"/api/conferences/{conference_vo_id}/"
            conf = ConferenceVO.objects.get(import_href=conference_href)
            new_attendee["conference"] = conf
        except ConferenceVO.DoesNotExist:
            return JsonResponse(
                {"message": "Invalid conference id"},
                status=400,
            )
        # Create and return the new attendee.
        attendee = Attendee.objects.create(**new_attendee)
        print(attendee)
        return JsonResponse(
            attendee,
            encoder=AttendeeDetailEncoder,
            safe=False,
        )


@require_http_methods(["DELETE", "GET", "PUT"])
def api_show_attendee(request, id):
    """
    Returns the details for the Attendee model specified
    by the id parameter.

    This should return a dictionary with email, name,
    company name, created, and conference properties for
    the specified Attendee instance.

    {
        "email": the attendee's email,
        "name": the attendee's name,
        "company_name": the attendee's company's name,
        "created": the date/time when the record was created,
        "conference": {
            "name": the name of the conference,
            "href": the URL to the conference,
        }
    }
    """
    if request.method == "GET":
        try:
            attendee = Attendee.objects.get(id=id)
            return JsonResponse(
                attendee,
                encoder=AttendeeDetailEncoder,
                safe=False,
            )
        except Attendee.DoesNotExist:
            return JsonResponse({"message": "Invalid attendee id"})
    elif request.method == "DELETE":
        # This block handles deleting an attendee.
        count, _ = Attendee.objects.filter(id=id).delete()
        return JsonResponse({"deleted": count > 0})
    else:
        # This block handles updating the attendee info.
        updated_attendee = json.loads(request.body)
        try:
            # Get the conference object and put it in the dict
            # This is going to have to change to use the conference vo href,
            # but I have not yet figured out how.
            conference = ConferenceVO.objects.get(
                id=updated_attendee["conference"]
            )
            updated_attendee["conference"] = conference
        except ConferenceVO.DoesNotExist:
            return JsonResponse(
                {"message": "Invalid conference id"},
                status=400,
            )
        # update the existing attendee
        Attendee.objects.filter(id=id).update(**updated_attendee)
        # return the updated attendee
        attendee = Attendee.objects.get(id=id)
        return JsonResponse(
            attendee,
            encoder=AttendeeDetailEncoder,
            safe=False,
        )
