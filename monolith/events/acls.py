import json
import requests
from .keys import PEXELS_API_KEY, OPEN_WEATHER_API_KEY


def get_loc_pic(city, state):
    # Using the Pexels API to get a photo for this location.
    url = "https://api.pexels.com/v1/search"
    parameters = {"query": f"{city}, {state}, downtown", "photos": 1}
    header = {"Authorization": PEXELS_API_KEY}
    response = requests.get(url, params=parameters, headers=header)
    content = json.loads(response.content)
    # clean the data received so this function only returns the specific
    # information requested
    return {"picture_url": content["photos"][0]["src"]["medium"]}


def get_conf_weather(city, state):
    # The response will be a dictionary.
    response = {}
    print(city)
    print(state)

    # The OpenWeather API requires a latitude and longitude for a weather
    # query. First we will get the latitude and longitude for the
    # requested city and state.
    # Currently our assumption is the only locations we are using are
    # United States cities, so we will hard-code the country code of US.
    url = "http://api.openweathermap.org/geo/1.0/direct"
    parameters = {
        "q": f"{city}, {state}, US",
        "limit": 2,
        "appid": {OPEN_WEATHER_API_KEY},
    }
    JSONcoords = requests.get(url, params=parameters)

    # Convert JSON response into a Python dictionary
    coords = json.loads(JSONcoords.content)

    # Get the latitude and longitude from the converted JSON response.
    lat = coords[0]["lat"]
    lon = coords[0]["lon"]

    # Use the returned coordinates to get the current weather.
    # Specify imperial units to get the temperature back in Farenheit.
    url = "https://api.openweathermap.org/data/2.5/weather"
    parameters = {
        "lat": lat,
        "lon": lon,
        "units": "imperial",
        "appid": {OPEN_WEATHER_API_KEY},
    }
    JSONweather = requests.get(url, params=parameters)

    # Convert JSON response into a Python dictionary
    weather = json.loads(JSONweather.text)

    # Get the temperature and description from the converted JSON response.
    temp = weather["main"]["temp"]
    description = weather["weather"][0]["description"]
    response["temp"] = temp
    response["description"] = description
    return response
