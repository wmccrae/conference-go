import json
import pika
import django
import os
import sys
from django.core.mail import send_mail


sys.path.append("")
os.environ.setdefault("DJANGO_SETTINGS_MODULE", "presentation_mailer.settings")
django.setup()


# That code loads the Django settings so that you can use the email system.
# The function send_mail is the one that you'll use.

# def process_message(ch, method, properties, body):
#     print("  Received %r" % body)


def process_approval(ch, method, properties, body):
    # Get the presenter and presentation info
    print(body)
    message = json.loads(body)
    name = message["presenter_name"]
    email = message["presenter_email"]
    title = message["title"]
    send_mail(
        "Your presentation has been accepted",
        f"{name}, we are happy to tell you that your presentation {title} has been accepted.",
        "admin@conference.go",
        [email],
        fail_silently=False,
    )
    print("Approval email sent.")


def process_rejection(ch, method, properties, body):
    # Get the presenter and presentation info
    print(body)
    message = json.loads(body)
    name = message["presenter_name"]
    email = message["presenter_email"]
    title = message["title"]
    send_mail(
        "Your presentation has been rejected",
        f"{name}, we are sorry to tell you that your presentation {title} has been rejected.",
        "admin@conference.go",
        [f"{email}"],
        fail_silently=False,
    )
    print("Rejection email sent.")


parameters = pika.ConnectionParameters(host="rabbitmq")
connection = pika.BlockingConnection(parameters)
channel = connection.channel()
channel.queue_declare(queue="presentation_approvals")
channel.basic_consume(
    queue="presentation_approvals",
    on_message_callback=process_approval,
    auto_ack=True,
)
channel.queue_declare(queue="presentation_rejections")
channel.basic_consume(
    queue="presentation_rejections",
    on_message_callback=process_rejection,
    auto_ack=True,
)
channel.start_consuming()
